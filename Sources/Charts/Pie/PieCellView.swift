//
//  PieChartCell.swift
//  Charts
//
//  Created by Brahand on 4/20/22.
//

import SwiftUI

public struct PieCellView: View {
    var center : CGPoint
    var radius : CGFloat
    var color  : Color
    var startAngle  : Angle
    var endAngle    : Angle

    var segmentPath: Path {
        var path = Path()
        path.addArc(
            center      : self.center,
            radius      : self.radius,
            startAngle  : self.startAngle,
            endAngle    : self.endAngle,
            clockwise   : false
        )
        path.addLine(to: center)
        path.closeSubpath()
        return path
    }

    public var body: some View {
        segmentPath
            .fill()
            .foregroundColor(color.opacity(0.8))
            .overlay(segmentPath.stroke(.white, lineWidth: 1))
    }
}
