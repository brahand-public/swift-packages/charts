//
//  PieChartView.swift
//  Charts
//
//  Created by Brahand on 4/20/22.
//

import SwiftUI

public struct PieChartView : View {
    private let values: [(amount: Double, color: Color)]

    private var segments: [(start: Angle, end: Angle)] = []

    public init(_ values: [(amount:Double, color: Color)]) {
        self.values = values

        let maximumValue = values.map(\.amount).reduce(0, +)
        var currentAngle = 0.0
        for value in values.map(\.amount) {
            let normalizedValue = value / maximumValue
            let startAngle = currentAngle
            let endAngle = currentAngle + (normalizedValue * 360)
            self.segments.append( (start: Angle(degrees: startAngle), end: Angle(degrees: endAngle)))
            currentAngle = endAngle
        }
    }

    public var body: some View {
        GeometryReader { geometry in
            ForEach(self.segments.indices, id: \.self){ i in
                PieCellView(
                    center: CGPoint(x: geometry.frame(in: .local).midX, y: geometry.frame(in: .local).midY),
                    radius: min(geometry.size.width, geometry.size.height) / 2.0,
                    color: self.values[i].color,
                    startAngle: self.segments[i].start,
                    endAngle: self.segments[i].end
                )
            }
        }
    }
}
