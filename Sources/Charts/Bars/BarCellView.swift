//
//  BarCellView.swift
//  Charts
//
//  Created by Brahand on 4/20/22.
//

import SwiftUI

public struct BarCellView : View {
    let normalizedValue : Double
    let cellWidth       : Double
    let cellHeight      : Double
    let gradient        : Gradient

    public var body: some View {
        Rectangle()
            .fill(
                LinearGradient(gradient: gradient, startPoint: .top, endPoint: .bottom)
            )
            .frame(width: self.cellWidth, height: cellHeight)
            .cornerRadius(4.0, corners: [.topLeft, .topRight])
    }
}

