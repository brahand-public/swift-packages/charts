//
//  BarChartView.swift
//  Charts
//
//  Created by Brahand on 4/20/22.
//

import SwiftUI

public struct BarChartView : View {
    private let values: [Double]
    private let gradient: Gradient

    private let maximumValue: Double
    private let normalizedValues: [Double]

    public init(_ values: [Double], gradient: Gradient) {
        self.values = values
        self.gradient = gradient

        var maximumValue = 1.0 // non-zero value
        if let maxValue = values.max(), !maxValue.isZero {
            maximumValue = maxValue
        }

        self.normalizedValues = values.map { $0 / maximumValue }
        self.maximumValue = maximumValue
    }

    public var body: some View {
        GeometryReader { geometry in
            HStack(alignment: .bottom, spacing: 0.0) {
                ForEach(normalizedValues.indices, id: \.self) { i in
                    BarCellView(
                        normalizedValue : normalizedValues[i],
                        cellWidth       : geometry.size.width / (Double(self.values.count) * 1.5),
                        cellHeight      : geometry.size.height * normalizedValues[i],
                        gradient        : self.gradient
                    )
                }
                .frame(minWidth: 0, maxWidth: .infinity)
            }
        }
    }
}

//struct BarChartView_Previews: PreviewProvider {
//    static var previews: some View {
//        Group {
//            BarChartView([10, 20, 5, 30, 50], gradient: Gradient.blue)
//            BarChartView([5, 10], gradient: Gradient.blue)
//            BarChartView([0], gradient: Gradient.blue)
//            BarChartView([10, 20, 5, 30, 50, 10, 100, 40, 60, 90, 80, 5, 10], gradient: Gradient.blue)
//        }
//        .previewLayout(.fixed(width: 300, height: 100))
//    }
//}
