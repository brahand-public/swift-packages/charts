//
//  Line.swift
//  Charts
//
//  Created by Brahand on 4/20/22.
//


import SwiftUI

public struct Line: View {
    let values: [Double]

    let frame: CGRect
    let minDataValue: Double?
    let maxDataValue: Double?
    let showBackground: Bool
    let gradient: Gradient
    let curvedLines: Bool

    let topPadding: CGFloat = 3.0

    var stepWidth: CGFloat {
        if values.count < 2 {
            return 0
        }
        return frame.size.width / CGFloat(values.count-1)
    }
    var stepHeight: CGFloat {
        var min: Double?
        var max: Double?
        let points = self.values
        if minDataValue != nil && maxDataValue != nil {
            min = minDataValue!
            max = maxDataValue!
        }else if let minPoint = points.min(), let maxPoint = points.max(), minPoint != maxPoint {
            min = minPoint
            max = maxPoint
        }else {
            return 0
        }
        if let min = min, let max = max, min != max {
            if (min <= 0){
                return (frame.size.height-topPadding) / CGFloat(max - min)
            }else{
                return (frame.size.height-topPadding) / CGFloat(max - min)
            }
        }
        return 0
    }

    var path: Path {
        if curvedLines {
            return Path.quadCurvedPathWithPoints(points: self.values, step: CGPoint(x: stepWidth, y: stepHeight), globalOffset: minDataValue)
        } else {
            return Path.linePathWithPoints(points: self.values, step: CGPoint(x: stepWidth, y: stepHeight), globalOffset: minDataValue)
        }
    }

    var closedPath: Path {
        if curvedLines {
            return Path.quadClosedCurvedPathWithPoints(points: self.values, step: CGPoint(x: stepWidth, y: stepHeight), globalOffset: minDataValue)
        } else {
            return Path.closedLinePathWithPoints(points: self.values, step: CGPoint(x: stepWidth, y: stepHeight), globalOffset: minDataValue)
        }
    }

    public var body: some View {
        if self.showBackground {
            self.closedPath
                .fill(LinearGradient(gradient: gradient, startPoint: .top, endPoint: .bottom))
                .rotationEffect(.degrees(180), anchor: .center)
                .rotation3DEffect(.degrees(180), axis: (x: 0, y: 1, z: 0))
                .transition(.opacity)
        }
        self.path
            .stroke(LinearGradient(gradient: gradient, startPoint: .leading, endPoint: .trailing), style: StrokeStyle(lineWidth: 3, lineJoin: .round))
            .rotationEffect(.degrees(180), anchor: .center)
            .rotation3DEffect(.degrees(180), axis: (x: 0, y: 1, z: 0))
    }
}
