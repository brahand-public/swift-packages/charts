//
//  LinesChartView.swift
//  Charts
//
//  Created by Brahand on 4/20/22.
//

import SwiftUI

public struct LinesChartView: View {
    let lines: [(amounts:[Double], color: Gradient)]
    let showBackground: Bool
    let showCurvedLines: Bool

    let globalMinimum: Double
    let globalMaximum: Double
    
    public init(_ lines: [(amounts:[Double], color: Gradient)], showBackground: Bool, showCurvedLines: Bool) {
        self.lines = lines
        self.showBackground = showBackground
        self.showCurvedLines = showCurvedLines

        self.globalMaximum = lines.flatMap(\.amounts).max() ?? 0
        self.globalMinimum = lines.flatMap(\.amounts).min() ?? 0
    }
    
    public var body: some View {
        GeometryReader{ geometry in
            ForEach(self.lines.indices, id: \.self) { i in
                Line(
                    values: self.lines[i].amounts,
                    frame: geometry.frame(in: .local),
                    minDataValue: self.globalMinimum,
                    maxDataValue: self.globalMaximum,
                    showBackground: self.showBackground,
                    gradient: self.lines[i].color,
                    curvedLines: self.showCurvedLines
                )
            }

        }
    }
}
